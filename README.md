# Desafio Qualidade

**Desafio QA**

Este desafio tem o intuito de avaliar suas habilidades em testes de E2E.

> Testes devem ser realizados em TestCafe e/ou Cypress, seguindo as boas práticas de decodificação.

**1. Descrição do projeto**

Site para testar: https://www.saucedemo.com/

Loja virtual com protudos diversos.

**2. Desafio a ser realizado:**
- Entrar com usuário pré-definido pela própria plataforma; 
- Navegar pelo menu, selecionar um produto e realizar uma compra na loja virtual;
- Verificar se o status da compra está correto no histórico da compra; 
- Montar a suíte de testes;

> Fique a vontade para criar novos casos de teste se preferir.

**3. Ao final o resultado do desafio deve ter:**

- Um relatório com todos os bugs encontrado caso haja algum, seguindo uma metodologia de sua preferência;
- Há bugs? Que decisão você tomaria para resolve-los? 
- Uma documentação de como executar o ambiente de teste;
- Qual técnica de teste foi utilizada e porque decidiu por ela.

**4. Instruções de entrega**

- Primeiro, faça um fork deste projeto para sua conta no GitLab (ou crie uma conta se não tiver);
- Em seguida, implemente o desafio seguindo as instruções descritas no item 2;
- Por fim, envie o link do seu Pull Request, com a resultado do desafio, para os e-mails: fvastres@zak.app e gbassan@zak.app.

**5. Avaliação**

Seu teste será avaliado de acordo com os seguintes critérios:

- Análise, planejamento e técnicas de teste. 
- Mapeamento de cenários de testes.
- Orientação a objetos.
- Estrutura dos testes automatizados (Preparação, Execução e Validação).
- Arquitetura do projeto de automação (PageObjects, Features, Step Definitions, Datapool, Dataprovider, Framework).
- Boas práticas de desenvolvimento de software (design pattern, clean code e etc).
- Simplicidade e objetividade.
